from django.db import models
from django.conf import settings

User = settings.AUTH_USER_MODEL


class Post(models.Model):
    image = models.CharField(max_length=254)
    description = models.CharField(max_length=128)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)


class Like(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    isLike = models.BooleanField(
        choices=[
            (True, 'Like'),
            (False, 'Dislike')
        ]
    )


class Read(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    status = models.CharField(
        default='Subscriber',
        max_length=10,
        choices=[
            ('Subscriber', 'Subscriber'),
            ('Registered', 'Registered'),
        ],
    )
