from rest_framework.permissions import BasePermission
from rest_framework.viewsets import ModelViewSet
from rest_framework import permissions
from rest_framework.decorators import action
from rest_framework.response import Response

from posts.api.serializers import PostSerializer
from posts.models import Post


class PostViewSet(ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer

    permission_classes = [permissions.IsAuthenticated]


    def get_serializer_class(self):
        serializer = self.serializer_class
        return serializer

    @action(methods=['POST'], detail=True, url_path='like')
    def like(self, request, *args, **kwargs):
        instance = self.get_object()
        queryset = instance.like_set.filter(user=self.request.user)
        if instance.like_set.filter(user=self.request.user, isLike=True):
            return Response('Лайк уже стоит')
        if instance.like_set.filter(user=self.request.user, isLike=False):
            instance.like_set.update(isLike=True, user=self.request.user)
            return Response('Сменил дизлайк на лайк')
        instance.like_set.create(isLike=True, user=self.request.user)
        return Response('Поставил лайк')

    @action(methods=['POST'], detail=True, url_path='dislike')
    def dislike(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.like_set.filter(user=self.request.user, isLike=False):
            return Response('Дизлайк уже стоит')
        if instance.like_set.filter(user=self.request.user, isLike=True):
            instance.like_set.update(isLike=False, user=self.request.user)
            return Response('Сменил лайк на дизлайк')
        instance.like_set.create(isLike=False, user=self.request.user)
        return Response('Поставил дизлайк')
