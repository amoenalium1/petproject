from rest_framework import permissions
from rest_framework.viewsets import ModelViewSet
from rest_framework.decorators import action
from rest_framework.response import Response

from accounts.api.serializers.User import *
from accounts.models import User


class CustomCheckPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        if not request.user.is_authenticated and view.action == 'create':
            return True

        return request.user.is_authenticated

class UserViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    permission_classes = [CustomCheckPermission]

    def get_serializer_class(self):
        serializer = self.serializer_class
        if self.action == 'list':
            if self.request.user.is_staff:
                serializer = UserAdminListSerializer
            if not self.request.user.is_staff:
                serializer = UserListSerializer
        elif self.action == 'create':
            serializer = UserCreateSerializer
        elif self.action == 'retrieve':
            serializer = UserRetrieveSerializer
        return serializer

    @action(methods=['POST'], detail=True, url_path='subscribe')
    def subscribe(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.subscribe.filter(id=self.request.user.id).exists():
            return Response('Уже существует')
        self.request.user.subscribes.add(instance)
        return Response('Подписался')

    @action(methods=['POST'], detail=True, url_path='unsubscribe')
    def unsubscribe(self, request, *args, **kwargs):
        instance = self.get_object()
        if not instance.subscribe.filter(id=self.request.user.id).exists():
            return Response('Id не существует')
        self.request.user.subscribes.remove(instance)
        return Response('Отписался')